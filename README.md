# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an app developed from a basic tutorial API integration in a flutter App

### How do I get set up? ###

* Summary of set up
	Command Line
	Run cd ~/{repo location}/flutter_notes/notes
	Make sure your device/emulator is running.	
	Run flutter run.
* Configuration
* Dependencies
	environment:sdk: ">=2.1.0 <3.0.0"
	dependencies:
  		flutter:
    		sdk: flutter
  		cupertino_icons: ^0.1.2
  		get_it: ^4.0.1
  		http: ^0.12.0+4
  		json_serializable: ^3.3.0
	dev_dependencies:
  		flutter_test:
    		sdk: flutter
  		build_runner: ^1.9.0
	flutter:.
  		uses-material-design: true

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact